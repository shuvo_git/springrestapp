package com.spring_rest.rest_app.model;

import javax.persistence.*;

/*
 * Table name : "employees"
 *
 * BY Jobayed Ullah, 17/06/2020
 */

@Entity
@Table(name="employees")
public class Employee
{
    // Properties...
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long   id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="email_id")
    private String emailId;

    // Constructors ...
    public Employee()
    {
        super();
    }
    public Employee(long id, String firstName, String lastName, String emailId)
    {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
    }


    // Getters and setters...
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastName;
    }

    public void setLastname(String lastname) {
        this.lastName = lastname;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
